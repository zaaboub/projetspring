package com.fr.adaming.entity;
import javax.persistence.*;

import java.io.Serializable;

@Entity
@DiscriminatorValue(value = "habillement") 
public class Habillement extends Produit  implements Serializable  {
	
	private long size ;
	private String couleur ;
	public Habillement(long size, String couleur) {
		super();
		this.size = size;
		this.couleur = couleur;
	}
	public Habillement() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Habillement(long code_produit, String nom_produit, Usermanger user) {
		super(code_produit, nom_produit, user);
		// TODO Auto-generated constructor stub
	}
	public long getSize() {
		return size;
	}
	public void setSize(long size) {
		this.size = size;
	}
	public String getCouleur() {
		return couleur;
	}
	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}
	
	
	

}
