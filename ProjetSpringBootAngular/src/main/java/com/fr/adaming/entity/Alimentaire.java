package com.fr.adaming.entity;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import java.io.Serializable;

@Entity
@DiscriminatorValue(value = "alimentaire")
public class Alimentaire extends Produit  implements Serializable  {
	  
	
	private long date_exploitation ;

	public long getDate_exploitation() {
		return date_exploitation;
	}

	public void setDate_exploitation(long date_exploitation) {
		this.date_exploitation = date_exploitation;
	}

	public Alimentaire(long date_exploitation) {
		super();
		this.date_exploitation = date_exploitation;
	}

	public Alimentaire() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	
	
	
	

}
