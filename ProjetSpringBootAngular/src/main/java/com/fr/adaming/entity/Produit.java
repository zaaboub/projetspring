package com.fr.adaming.entity;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;


@Table(name="produit")
@DiscriminatorColumn(name = "type_produit")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Entity
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({
@JsonSubTypes.Type(value = Habillement.class, name = "Produit Habillement"),
@JsonSubTypes.Type(value = Alimentaire.class, name = "Produit Alimentaire")
}) 
public class Produit implements Serializable  {
	
	@Id
	private long code_produit;
	private String nom_produit ;
	@ManyToOne(cascade=CascadeType.MERGE, fetch = FetchType.EAGER)
	
	private Usermanger user;
	

	public Produit() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Produit(long code_produit, String nom_produit, Usermanger user) {
		super();
		this.code_produit = code_produit;
		this.nom_produit = nom_produit;
		this.user = user;
	}
	public long getCode_produit() {
		return code_produit;
	}
	public void setCode_produit(long code_produit) {
		this.code_produit = code_produit;
	}
	public String getNom_produit() {
		return nom_produit;
	}
	public void setNom_produit(String nom_produit) {
		this.nom_produit = nom_produit;
	}
	
	
	public Usermanger getUser() {
		return user;
	}
	public void setUser(Usermanger user) {
		
		this.user = user;
	}

	
	

}
